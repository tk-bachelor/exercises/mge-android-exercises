package com.keerthikan.helloworld;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnOpen = (Button) findViewById(R.id.btnOpen);
        btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(i);
            }
        });

        this.stateChangedTo("Created");
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.stateChangedTo("Started");
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.stateChangedTo("Resumed");
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.stateChangedTo("Paused");
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.stateChangedTo("Stopped");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.stateChangedTo("Destroyed");
    }

    private void stateChangedTo(String state) {
        String currentTimeString = getCurrentTimeString();

        Notification notification =
                new Notification.Builder(this)
                        .setContentTitle(state)
                        .setSmallIcon(android.R.drawable.presence_busy)
                        .setContentText(currentTimeString)
                        .getNotification();
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify((int) System.currentTimeMillis(), notification);

        Log.d(getString(R.string.app_name), state + " at " + currentTimeString);
    }

    private String getCurrentTimeString() {
        return new SimpleDateFormat("HH:mm:ss.SSS").format(new Date());
    }

}
