package com.keerthikan.layoutapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentationTest {

    @Rule
    public ActivityTestRule mainActivityRule = new ActivityTestRule(MainActivity.class);

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.keerthikan.layoutapp", appContext.getPackageName());
    }

    @Test
    public void testEmail() {
        MainActivity activity = (MainActivity) mainActivityRule.getActivity();
        final EditText etEmail = (EditText) activity.findViewById(R.id.etEmail);

        String email = "hello@example.com";
        onView(withId(R.id.etEmail))
                .perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.btnLogin))
                .perform(click());

        // check fromEmail
        onView(withId(R.id.etFromEmail))
                .check(matches(withText(email)));
    }
}
