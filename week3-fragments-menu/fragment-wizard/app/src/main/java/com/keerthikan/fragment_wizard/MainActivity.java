package com.keerthikan.fragment_wizard;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.keerthikan.fragment_wizard.fragments.StepHelloFragment;
import com.keerthikan.fragment_wizard.fragments.StepNameFragment;
import com.keerthikan.fragment_wizard.fragments.StepNewsletterFragment;
import com.keerthikan.fragment_wizard.model.UserRegistrationData;

import java.util.Collections;

public class MainActivity extends AppCompatActivity implements
        StepHelloFragment.OnStepHelloFragmentListener,
        StepNameFragment.OnStepNameFragmentListener,
        StepNewsletterFragment.onStepNewsletterFragmentListner {
    public static final String USER_DATA = "USER_DATA";
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.fragmentManager = getFragmentManager();
    }

    @Override
    public void onRegister() {
        Fragment fragment = StepNameFragment.newInstance();
        this.switchFragment(fragment);
    }

    @Override
    public void onNext(String name) {
        UserRegistrationData userData = new UserRegistrationData();
        userData.setName(name);

        Fragment fragment = StepNewsletterFragment.newInstance(userData);
        switchFragment(fragment);
    }

    @Override
    public void onNewsletterNext(UserRegistrationData userData) {
        Toast.makeText(this, String.format("%s did %s subscribe", userData.getName(), userData.isNewsletter() ? "" : "not"), Toast.LENGTH_LONG).show();
    }

    private void switchFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
                .addToBackStack(fragment.getClass().getName())
                .commit();
    }
}
