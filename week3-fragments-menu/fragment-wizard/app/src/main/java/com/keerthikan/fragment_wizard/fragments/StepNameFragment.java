package com.keerthikan.fragment_wizard.fragments;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.support.design.widget.Snackbar;

import com.keerthikan.fragment_wizard.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StepNameFragment.OnStepNameFragmentListener} interface
 * to handle interaction events.
 * Use the {@link StepNameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StepNameFragment extends Fragment {
    private OnStepNameFragmentListener mListener;

    public StepNameFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment StepNameFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StepNameFragment newInstance() {
        StepNameFragment fragment = new StepNameFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_step_name, container, false);

        final EditText etName = (EditText) rootView.findViewById(R.id.nameEditText);
        Button btnNext = (Button) rootView.findViewById(R.id.nextButton);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etName.getText().toString();
                if (name.isEmpty()) {
                    Snackbar.make(view, "Name is empty", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {
                    onNextButtonClicked(name);
                }
            }
        });
        return rootView;
    }

    public void onNextButtonClicked(String name) {
        if (mListener != null) {
            mListener.onNext(name);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnStepNameFragmentListener) {
            mListener = (OnStepNameFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement onStepNewsletterFragmentListner");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnStepNameFragmentListener {
        void onNext(String name);
    }
}
