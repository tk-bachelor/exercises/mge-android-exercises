# MGE - Android Exercises

Github: https://github.com/HSR-MGE

## Week 1 - [Introduction and Android Concepts](https://docs.google.com/presentation/d/1VnW43vWOMMOslqA7fmBvLfRviwNlCNiTXQM21AhwbKk/edit)

Exercises: [Activities and Intents](./week1-introduction)

Self-Study:
* [Getting Started with Testing](https://developer.android.com/training/testing/start/index.html)
* [Building Local Unit Tests](https://developer.android.com/training/testing/unit-testing/local-unit-tests.html)

## Week 2 - [Basics of GUI Programming](https://docs.google.com/presentation/d/1tb-dj9ZSPiaYXGIRMvbM1lN18KTmAMkSK93f19S6Zok/edit#slide=id.p)

Exercises: [Layouts and Views](./week2-layout-events)

Self-Study:
* [Building Instrumented Unit Tests](https://developer.android.com/training/testing/unit-testing/instrumented-unit-tests.html)
* [Automating User Interface Tests](https://developer.android.com/training/testing/ui-testing/index.html)

## Week 3 - [Structures and Navigation](https://docs.google.com/presentation/d/1ulzBi2fpOOTSRnb-nIdhZ7DC6khVDUPl24L9DaOS8Jc/edit#slide=id.p)

Exercises: [Fragments and Menus](./week3-fragments-menu)

Self-Study:
* [Designing Effective Navigation](https://developer.android.com/training/design-navigation/index.html)
* [Navigation Patterns](https://developer.android.com/design/patterns/navigation.html)

## Week 4 - [Master Detail Navigation](https://docs.google.com/document/d/1oyiUe3dbxs4jollImR3WQOPt1JDA1-tHQGNPA1989Wg/edit)

Exercises: [Master Detail](./week4-master-details)

Self-Study:
* [Card View](https://developer.android.com/training/material/lists-cards.html#CardView)
* [How to avoid loading indicators](https://www.youtube.com/watch?v=Rehw8L5Cl8M)

## Week 5 - [Material Design](https://docs.google.com/document/d/16EDUGxusPL5OvW80TmYP5bQSzcNlWo8PuRDxSXQXvY8/edit)

Self-Study:
* [Material Design in Google I/O App](https://www.youtube.com/watch?v=XOcCOBe8PTc)
* [Android Support Library 23.2](https://android-developers.googleblog.com/2016/02/android-support-library-232.html)

# Week 6 - [Hintergrunddienste](https://docs.google.com/document/d/1XljfeOiRKWNXl5RMqp2ExZRcOR9HC7cXJQhYA7tKLao/edit)

Self-Study:
* [UX HowTo by Intel Software](https://www.youtube.com/playlist?list=PLg-UKERBljNy2Yem3RJkYL1V70dpzkysC)